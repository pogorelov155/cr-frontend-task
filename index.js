var data = {
	"instances": [
		{
			"text": "VM1",
			"nodes": [
				{
					"text": "NIC1.1",
					"aws": {
						"availability_zone" : "us-west-2c",
						"subnet": "10.10.10.0/24"
					}
				},
			]
		},
		{
			"text": "VM2",
			"nodes": [
				{
					"text": "NIC1",
					"aws": {
						"availability_zone" : "us-west-2c",
						"subnet": "10.10.10.0/24"
					}
				},
				{
					"text": "NIC2",
					"aws": {
						"availability_zone" : "us-west-2c",
						"subnet": "10.10.10.0/24"
					}
				},
				{
					"text": "NIC3",
					"aws": {
						"availability_zone" : "us-west-2c",
						"subnet": "10.10.30.0/24"
					}
				}
			]
		},
		{
			"text": "VM3",
			"nodes": [
				{
					"text": "NIC3.1",
					"aws": {
						"availability_zone" : "us-west-2a",
						"subnet": "10.10.20.0/24"
					}
				},
				{
					"text": "NIC2.3",
					"aws": {
						"availability_zone" : "us-west-2c",
						"subnet": "10.10.10.0/24"
					}
				},
				{
					"text": "NIC3.4",
					"aws": {
						"availability_zone" : "us-west-2r",
						"subnet": "12.13.30.0/24"
					}
				}
			]
		},
		{
			"text": "VM4",
			"nodes": [
				{
					"text": "NIC4.1",
					"aws": {
						"availability_zone" : "us-west-2c",
						"subnet": "10.10.30.0/24"
					}
				},
			]
		}
	]
};

$(function () {
	// cache
	var tree = $('#tree'),
		selectedItem = $('#selected-item'),
		groupByHolder = $('#group-by-holder');

	// this object will contain all filters.
	var database = {
		all: {'null':{}}
	};

	// this object will all VMs
	var vms = {};

	// @param {Integer} how much characters will be in string
	// @return {String}
	function randomString (length) {
		return Math.random()
			.toString(36)
			.replace(/[^a-z]+/g, '')
			.substr(0, length);
	}


	// @param {Object} VM
	// @return {String} HTML code for node view
	function templateNode (instance) {
		var result = '',
			key;
		if (!instance.aws) {
			result += '<h1>' + instance.text + '</h1>';
			result += '<p>';
			result += '<b>Nodes: </b>';
			result += '<span>' + instance.nodes.length + '</span>';
			result += '</p>';
			return result;
		}
		result += '<h1>' + instance.text + '</h1>';
		for (key in instance.aws) {
			result += '<p>';
			result += '<b>' + key + ': </b>';
			result += '<span>' + instance.aws[key] + '</span>';
			result += '</p>';
		}
		return result;
	}

	function onNodeSelectedHandler (e, obj) {
		selectedItem.html(templateNode(obj));
	}

	// start point
	function renderByGroupHandler (group) {
		var filtered = {},
			renderData = [],
			tempNode,
			key,
			instance;

		if (!group) {
			group = 'all';
		}

		if (!database[group]) {
			console.warn('Value ' + group + ' is not correct');
			return;
		}

		for (key in database[group]) {
			tempNode = {
				'text': key,
				color: "#5e73d1",
				icon: "glyphicon glyphicon-folder-open",
				nodes: []
			};
			for (instance in database[group][key]) {
				tempNode.nodes.push(database[group][key][instance]);
			}
			if (key === 'null') {
				renderData = tempNode.nodes;
			} else {
				renderData.push(tempNode);
			}
		}

		tree.treeview({
			data: renderData,
			tags: true,
			onNodeSelected: onNodeSelectedHandler
		});
	}

	// fills "database" object
	// @param {Object} instance - VM
	function parseNodes (instance) {
		var nodes = instance.nodes,
			node,
			i,
			key;
		instance = jQuery.extend(true, {}, instance );
		for (i = 0; i < nodes.length; i++) {
			node = nodes[i];

			for (key in node.aws) {
				if (!database[key]) {
					database[key] = {};
				}
				if (!database[key][node.aws[key]]) {
					database[key][node.aws[key]] = {};
				}

				// performance optimization
				if (!database[key][node.aws[key]][instance.text]) {
					// clone instance
					database[key][node.aws[key]][instance.text] = jQuery.extend(
						{},
						instance,
						{nodes: instance
							.nodes
							.filter(function (aws) {
								return node.aws[key] === aws.aws[key];
							})
						}
					);
				}
			}

		}
	}

	// @param {Object}
	// @param {Object}
	// @return {Array} keys[]: string. There is keys, that coincident in both objects
	function coincidentKeys (obj1, obj2) {
		var obj = [],
			i,
			key;
		for (key in obj1) {
			if (obj2[key]) {
				obj.push(key);
			}
		}
		return obj;
	}

	// creates new branch in "database"
	// @param {String} group name
	// @param {String} group name
	function combineGroups (group1, group2) {
		if (!database[group1]) {
			console.warn('There is no "' + group1 + '" group in database.');
			return false;
		}
		if (!database[group2]) {
			console.warn('There is no "' + group2 + '" group in database.');
			return false;
		}

		var groupRoot,
			groupName,
			groupItems,
			instance,
			i,
			key1,
			key2;

		groupRoot = database[group1 + ' / ' + group2] = {};
		for (key1 in database[group1]) {
			for (key2 in database[group2]) {
				groupItems = coincidentKeys(database[group1][key1], database[group2][key2]);
				if (groupItems.length) {
					groupName = key1 + ' / ' + key2;

					if (!groupRoot[groupName]) {
						groupRoot[groupName] = {};
					}

					for (i = 0; i < groupItems.length; i++) {
						// performance optimization
						if (!groupRoot[groupName][groupItems[i]]) {
							instance = jQuery.extend(
								{},
								vms[groupItems[i]],
								{nodes: vms[groupItems[i]]
									.nodes
									.filter(function (node) {
										return node.aws[group1] === key1 && node.aws[group2] === key2;
									})
								}
							);
							if (instance.nodes.length > 0) {
								groupRoot[groupName][groupItems[i]] = instance;
							}

						}
					}
					if (Object.keys(groupRoot[groupName]).length === 0) {
						delete groupRoot[groupName];
					}
				}
			}
		}
	}

	// fill vms and database objects
	for (var i = 0; i < data.instances.length; i++) {
		var instance = data.instances[i];
		var vmName = instance.text;
		vms[vmName] = instance;

		instance.icon = 'glyphicon glyphicon-user';

		if (database.all['null'][vmName]) {
			instance.text += '_' + randomString(4);
			vmName = instance.text;
		}
		database.all['null'][vmName] = instance;

		parseNodes(instance);
	}

	// generate combined filter
	combineGroups('availability_zone', 'subnet');

	// choose handler by name attr
	groupByHolder.on('change', function (e) {
		var target = e.target;
		switch (target.name) {
			case 'groupBy':
				renderByGroupHandler(target.value);
		}
		updateGraph();
	});

	// fill filter field
	for (var type in database) {
		groupByHolder.find('[name="groupBy"]').append('<option>' + type + '</option>');
	}
	renderByGroupHandler();
	// console.log("database", database);
});



(function () {
	var schemeHolder = document.querySelector('#scheme-content-holder'),
		tree = document.querySelector('#tree'),
		star = document.createElement('div');

		star.id="scheme-star";
		star.className="scheme-star";

	window.addEventListener('resize', updateGraph);
	tree.parentNode.addEventListener('scroll', updateGraph);
	tree.parentNode.addEventListener('click', updateGraph);
	updateGraph();

	function updateGraph () {
		schemeHolder.innerHTML = ' ';
		schemeHolder.appendChild(star);

		var i,
			folders = tree.querySelectorAll('.glyphicon-folder-open'),
			posLeft = 0,
			starLeft = schemeHolder.clientWidth - star.clientWidth,
			starTop = star.offsetTop + 30;

		// use for of
		for (i = 0; i < folders.length; i++) {

			UpdatePatch (
				CreatePatch(schemeHolder),
				folders[i].getBoundingClientRect().top + document.body.scrollTop - offsetTop(schemeHolder),
				starLeft,
				starTop + 10,
				0,
				folders[i].parentNode.textContent
			)
		}

		schemeHolder.innerHTML = ' ' + schemeHolder.innerHTML;

	}

	window.updateGraph = updateGraph;

	function CreatePatch (parent) {
		var svg = document.createElement('svg');
		if (parent) {
			parent.appendChild(svg);
		}
		return svg;
	}

	function CreateLine (x1, x2, y1, y2) {
		return '<line x1="' + x1 + '" x2="' + x2 + '" y1="' + y1 + '" y2="' + y2 + '" stroke="#cccccc" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></line>';
	}
	function CreateText (x, y, text) {
		return '<text x="' + x + '" y="' + y + '" font-size="14" text-anchor="left" fill="#333333">' + text + '</text>';
	}

	function UpdatePatch (svg, top, right, bottom, left, text) {
		svg.style.position = 'absolute';
		var line =
			CreateLine(0, 200, 20, 20) +
			CreateLine(200, (right - left - 2), 20, (bottom - top - 2)) +
			CreateText(8, 16, text || '');

		if (bottom < top) {
			var tempTop = top;
			var tempLeft = left;
			top = bottom;
			bottom = tempTop;

			line =
				CreateLine(0, 200, (bottom - top + 18), (bottom - top + 18)) +
				CreateLine(200, (right - left - 2), (bottom - top + 18), 0) +
				CreateText(8, (bottom - top + 14), text || '');
		}

		svg.style.top = top + 'px';
		svg.style.left = left + 'px';
		svg.style.height = bottom - top + 20 + 'px';
		svg.style.width = right - left + 20 + 'px';
		svg.innerHTML = line;
	}

	function offsetTop (el) {
		var rect = el.getBoundingClientRect();
		var top;
		top = rect.top + window.pageYOffset || document.body.scrollTop;
		return top;
	};
})();
